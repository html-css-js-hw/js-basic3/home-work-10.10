// Завдання 1
const learnMoreLink = document.createElement("a");
learnMoreLink.textContent = "Learn More";
learnMoreLink.href = "#";

const footer = document.querySelector("footer");
const paragraph = footer.querySelector("p");

footer.insertBefore(learnMoreLink, paragraph.nextSibling);

// Завдання 2
const selectElement = document.createElement("select");
selectElement.id = "rating";

const main = document.querySelector("main");
const featuresSection = main.querySelector(".features");

main.insertBefore(selectElement, featuresSection);

const ratings = [
    { value: "4", text: "4 Stars" },
    { value: "3", text: "3 Stars" },
    { value: "2", text: "2 Stars" },
    { value: "1", text: "1 Star" },
];

ratings.forEach((rating) => {
    const optionElement = document.createElement("option");
    optionElement.value = rating.value;
    optionElement.textContent = rating.text;
    selectElement.appendChild(optionElement);
});
